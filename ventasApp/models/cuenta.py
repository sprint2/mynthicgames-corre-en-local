from django.db import models
from .usuarios import User

class Account(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='Cuenta', on_delete=models.CASCADE) #Atributo llave foranea. Realiza referencia al modelo User de user.py
    balance = models.IntegerField(default=0)
    lastChangeDate = models.DateTimeField()
    isActive = models.BooleanField(default=True)