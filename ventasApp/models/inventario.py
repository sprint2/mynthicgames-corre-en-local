from django.db import models

class inventariobd(models.Model):
    id_consola = models.AutoField(primary_key=True)
    marca = models.CharField(max_length=50)
    modelo = models.CharField(max_length=50)
    precio = models.FloatField()
    descuento = models.FloatField()
    descripcion = models.CharField(max_length=100)
    cantidad = models.IntegerField()