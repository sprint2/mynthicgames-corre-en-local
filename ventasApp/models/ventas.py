from django.db import models
from django.db.models.fields.related import ForeignKey

from mynthic.settings import DATE_INPUT_FORMATS

class Ventas(models.Model):
    id_factura = models.AutoField(primary_key = True)
    id_usuario = models.IntegerField(ForeignKey)
    id_consola = models.IntegerField(ForeignKey)
    cantidad = models.IntegerField()
    precio = models.FloatField()
    transportadora = models.CharField(max_length=200)
    direccion = models.CharField(max_length=400)
    metodo_pago = models.CharField(max_length=200)
    fecha = models.DateField(DATE_INPUT_FORMATS)



