from rest_framework import serializers
from ventasApp.models.inventario import inventariobd

class inventariobdSerializer(serializers.ModelSerializer):
    class Meta:
        model = inventariobd
        fields = ['id_consola', 'marca', 'modelo', 'precio', 'descuento', 'descripcion', 'cantidad']
