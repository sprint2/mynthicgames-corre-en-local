from django.db.models.base import Model
from rest_framework import serializers
from ventasApp.models.ventas import Ventas

class VentasSerializer(serializers.ModelSerializer):
    class Meta:
        Model = Ventas
        fields = ['id_factura', 'id_usuario','id_consola', 'cantidad', 
                  'precio', 'transportadora', 'direccion', 'metodo_pago', 'fecha']
                 
