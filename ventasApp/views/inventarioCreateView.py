from django.http.response import ResponseHeaders
from django.views import generic
from rest_framework.permissions import IsAuthenticated
from ventasApp.models import inventariobd
from ventasApp.serializers import inventarioSerializer
from ventasApp.serializers.inventarioSerializer import inventariobdSerializer
from rest_framework.response import Response
from rest_framework import generics, status

from rest_framework import status
class InventarioCreateView(generics.RetrieveAPIView):
    queryset = inventariobd.objects.all()
    serializer_class = inventariobd
    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
            snippets = inventariobd.objects.all()
            serializer = inventarioSerializer(snippets, many=True)
            return Response(serializer.data)

    def post(self, request, format=None):
        serializer = inventarioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
            snippet = self.get_object(pk)
            serializer = inventarioSerializer(snippet, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return ResponseHeaders(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)