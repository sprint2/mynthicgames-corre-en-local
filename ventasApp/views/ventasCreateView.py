from django.http.response import ResponseHeaders
from django.views import generic
from rest_framework.permissions import IsAuthenticated
from ventasApp.models import Ventas
from ventasApp.serializers import ventasSerializer
from ventasApp.serializers.ventasSerializer import VentasSerializer
from rest_framework.response import Response
from rest_framework import generics, status

from rest_framework import status
class VentasCreateView(generics.RetrieveAPIView):
    queryset = Ventas.objects.all()
    serializer_class = ventasSerializer
    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
            snippets = Ventas.objects.all()
            serializer = VentasSerializer(snippets, many=True)
            return Response(serializer.data)

    def post(self, request, format=None):
        serializer = VentasSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
            snippet = self.get_object(pk)
            serializer = VentasSerializer(snippet, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return ResponseHeaders(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)